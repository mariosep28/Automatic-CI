﻿public abstract class Controller
{
    protected abstract void RegisterCallbacks();
    protected abstract void UnregisterCallbacks();

    public void OnDestroy()
    {
        UnregisterCallbacks();
    }
}