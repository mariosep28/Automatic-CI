using UnityEngine.UIElements;

public abstract class View : VisualElement
{
    protected Controller controller;

    public View()
    {
        RegisterCallback<DetachFromPanelEvent>((evt) => OnDestroy());
    }

    protected abstract void SetVisualElements();

    protected abstract void RegisterCallbacks();

    protected abstract void UnregisterCallbacks();
    
    private void OnDestroy()
    {
        UnregisterCallbacks();
        
        controller?.OnDestroy();
    }
}