using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;

public class ConfigurationContentController : Controller
{
    public static AutomaticCISO ConfigurationTmp { get; private set; }
    
    private ConfigurationContentView _view;

    public ConfigurationContentController(ConfigurationContentView view)
    {
        _view = view;
        
        RegisterCallbacks();
        
        UpdateConfigurationContent(AutomaticCISettings.instance.Configuration);
    }

    protected override void RegisterCallbacks()
    {
        EventBus.onChangesReverted += UpdateConfigurationContent;
        EventBus.onChangesApplied += UpdateConfigurationContent;
    }

    protected override void UnregisterCallbacks()
    {
        EventBus.onChangesReverted -= UpdateConfigurationContent;
        EventBus.onChangesApplied -= UpdateConfigurationContent;
    }

    private void UpdateConfigurationContent(AutomaticCISO configuration)
    {
        _view.Unbind();

        if(configuration == null)
            _view.HideConfigurationContent();
        else
        {
            ConfigurationTmp = ScriptableObject.Instantiate(configuration);
            
            _view.Bind(new SerializedObject(ConfigurationTmp));
            
            _view.ShowConfigurationContent();
        }
    }
}