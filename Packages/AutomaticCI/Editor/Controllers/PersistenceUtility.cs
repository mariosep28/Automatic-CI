﻿using UnityEditor;
using UnityEngine;

public static class PersistenceUtility
{
    public static void Copy(Object objectToCopy, Object objectToChange)
    {
        var json = JsonUtility.ToJson(objectToCopy);
        JsonUtility.FromJsonOverwrite(json, objectToChange);

        EditorUtility.SetDirty(objectToChange);
    }
}