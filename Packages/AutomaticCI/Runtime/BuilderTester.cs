﻿using UnityEngine;

public class BuilderTester : MonoBehaviour
{
    [SerializeField] private AutomaticCISO configuration;
    
    [ContextMenu("BuildAutomaticCIFile")]
    public void BuildAutomaticCIFile()
    {
        var builder = new AutomaticCIBuilder();
        
        if (configuration.editMode || configuration.playMode)
            builder.WithTests(configuration.editMode, configuration.playMode);

        builder.WithBuilds(configuration.GetBuildPlatforms());
        builder.Save();
    }
}