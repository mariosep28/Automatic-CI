#!/usr/bin/env bash

if [[ -z "${UNITY_USERNAME}" ]] || [[ -z "${UNITY_PASSWORD}" ]]; then
  echo "UNITY_USERNAME or UNITY_PASSWORD environment variables are not set, please refer to instructions in the readme and add these to your secret environment variables."
  exit 1
fi

curl -sL https://deb.nodesource.com/setup_17.x | bash -
apt-get install -y nodejs

export PUPPETEER_SKIP_DOWNLOAD='true'
npm i puppeteer
apt update
apt install --assume-yes chromium-browser